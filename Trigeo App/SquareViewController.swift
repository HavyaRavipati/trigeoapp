//
//  SquareViewController.swift
//  Trigeo App
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class SquareViewController: UIViewController {
    let square_questions = ["Can we consider the above displayed square as parallelogram?", "What is the perimeter of a square in the above figure?", "Which of the following best describes the area of the figure?", ""]
    let square_answers = [["Yes!!", "No!!", "May be", "Can't be described from the information available"], ["4DC", "2AC", "2AB", "AD"], ["AB*AB", "AC*AC", "AB+AB", "4AC"]]
    var score:Int = 0
    var present_Q = 0
    var correct_A:UInt32 = 0
   
    @IBOutlet weak var scoreLBL: UILabel!
    @IBOutlet weak var SquQLBL: UILabel!
    @IBAction func answerBTN(_ sender: AnyObject) {
        if (sender.tag == Int(correct_A)){
            print("Its a correct answer!!")
            score += 1
            scoreLBL.text! = "\(score)"
            if (present_Q != square_questions.count){
                nextQuestion()
            }else{
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
            
        }
        else{
            
            let alert = UIAlertController(title: "Oops!!", message: "The Answer is : \(square_answers[present_Q-1][0])", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if (present_Q != square_questions.count){
                nextQuestion()
                
            }else{
                
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
        }
        
    }
    func displayCorrectAnswer() {
        let alert = UIAlertController(title: "Congrats!!", message: "Your score is\(score)\nClick next to goto next level", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var numberLBL: UILabel!
    func nextQuestion(){
        SquQLBL.text = square_questions[present_Q]
        numberLBL.text = String(present_Q+1)+": "
        correct_A = arc4random_uniform(4)+1
        var button:UIButton = UIButton()
        var init_tag = 1
        for i in 1...4{
            button = view.viewWithTag(i) as! UIButton
            if (i == Int(correct_A)){
                button.setTitle(square_answers[present_Q][0], for: .normal)
            }
            else{
                button.setTitle(square_answers[present_Q][init_tag], for: .normal)
                init_tag = init_tag + 1
            }
        }
        present_Q += 1
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        nextQuestion()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
