//
//  IntermediateViewController.swift
//  Trigeo App
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class IntermediateViewController: UIViewController {
    
    
    let intermediate_questions = ["if volume of a cube is 64 units, what is the side of the cube?", "what is the volume of cuboid with length as 3, breadth as 4, height as 5?", "If the area of a circle is 154, what is the diameter of that circle?", "If the radius of the cylinder is 14 and the height is 7, what is the volume of the cylinder?", "if two angles of a triangle measure to be 45, what is the third angle?"]
    let intermediate_answers = [["4", "3", "6", "8"], ["60", "30", "45", "None of the above"], ["14", "7", "6", "None of the above!"], ["4312", "4321", "4432", "4331"], ["90", "70", "180", "60"]]
    var score:Int = 0
    var present_Q = 0
    var correct_A:UInt32 = 0

    @IBOutlet weak var numberLBL: UILabel!
    @IBOutlet weak var scoreLBL: UILabel!
    @IBOutlet weak var intermediateLBL: UILabel!

    @IBAction func action(_ sender: AnyObject) {
        if (sender.tag == Int(correct_A)){
            print("Its a correct answer!!")
            score += 1
            scoreLBL.text! = "\(score)"
            if (present_Q != intermediate_questions.count){
                nextQuestion()
            }else{
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
            
        }
        else{
            print("OOPS! Its a wrong answer!!!!")
            
            
            let alert = UIAlertController(title: "Oops!!", message: "The Answer is : \(intermediate_answers[present_Q-1][0])", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if (present_Q != intermediate_questions.count){
                nextQuestion()
                
            }else{
                
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
        }
        
    }
    func displayCorrectAnswer() {
        let alert = UIAlertController(title: "Congrats!!", message: "Your score is\(score)\nClick Exit", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        nextQuestion()
    }
    func nextQuestion(){
        intermediateLBL.text = intermediate_questions[present_Q]
        numberLBL.text = String(present_Q+1)+": "
        correct_A = arc4random_uniform(4)+1
        var button:UIButton = UIButton()
        var init_tag = 1
        for i in 1...4{
            button = view.viewWithTag(i) as! UIButton
            if (i == Int(correct_A)){
                button.setTitle(intermediate_answers[present_Q][0], for: .normal)
            }
            else{
                button.setTitle(intermediate_answers[present_Q][init_tag], for: .normal)
                init_tag = init_tag + 1
            }
        }
        present_Q += 1
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

