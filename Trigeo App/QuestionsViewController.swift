//
//  QuestionsViewController.swift
//  Trigeo App
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    let answers:[String] = ["Square","Rectangle","Triangle","Circle"]
    var input:String = " "
    
    var answerkey:[String] = [String]()

    @IBOutlet weak var answerPK: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.answerPK.delegate = self
        self.answerPK.dataSource = self
        answerkey = Questionnaire.answers.sorted()
        
        // Do any additional setup after loading the view.
    }
    
    func pickerView(_ pickerView:UIPickerView,numberOfRowsInComponent component:Int) -> Int {
        return answerkey.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return answers[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        input = answers[row]
    }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


