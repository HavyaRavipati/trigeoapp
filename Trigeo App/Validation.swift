//
//  Validation.swift
//  Trigeo App
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import Foundation
struct Questionnaire {
    static let answers:[String] = ["Square","Rectangle","Triangle","Circle"]
    
    
    func AnswerLevel1(answer: String) -> String{
        if answer == "Rectangle"{
            return "Correct Answer"
        }else{
            return "Try Again"
        }
    }
}

