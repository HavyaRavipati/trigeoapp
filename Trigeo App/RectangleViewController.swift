//
//  RectangleViewController.swift
//  Trigeo App
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class RectangleViewController: UIViewController {
    
    let rectangle_questions = ["What is the displayed figure?", "Is the side AD = BC?", "What is the angle at ADC?", "if side AB = 5 units and side BC = 4 units, what is the perimeter of the rectangle?", "if side AB = 4 units and side BC = 3 units, what is the length of diagonal BD ?", "if length AB = 4 and length of the diagonal AC = 5, What is the area of Rectangle? "]
    let rectangle_answers = [["Rectangle", "Square", "Circle", "Rombus"], ["Yes!", "No!", "May be", "Can't be describe from the given information"], ["90", "180", "45", "270"], ["18", "20", "24", "30"], ["5", "3", "1", "4"], ["12", "24", "48", "36"]]
    
    var present_Q = 0
    var correct_A:UInt32 = 0

    var selectedLevel : String!
    override func viewDidLoad() {
       
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var numberLBL: UILabel!
    override func viewDidAppear(_ animated: Bool) {
        nextQuestion()
    }
    
    var score: Int = 0
    @IBOutlet weak var RectQLBL: UILabel!
    
    @IBOutlet weak var ScoreLBL: UILabel!
    
    @IBAction func AnswerBTN(_ sender: AnyObject) {
        
        if (sender.tag == Int(correct_A)){
            print("Its a correct answer!!")
            score += 1
             ScoreLBL.text! = "\(score)"
            if (present_Q != rectangle_questions.count){
                nextQuestion()
            }else{
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
            
        }
        else{
            print("OOPS! Its a wrong answer!!!!")
           
                
            let alert = UIAlertController(title: "Oops!!", message: "The Answer is : \(rectangle_answers[present_Q-1][0])", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if (present_Q != rectangle_questions.count){
                nextQuestion()
                
            }else{
                
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
        }
        
    }
    
    func displayCorrectAnswer() {
        let alert = UIAlertController(title: "Congrats!!", message: "Your score is\(score)\nClick next to goto next level", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func nextQuestion(){
        RectQLBL.text = rectangle_questions[present_Q]
        numberLBL.text = String(present_Q+1)+": "
        correct_A = arc4random_uniform(4)+1
        var button:UIButton = UIButton()
        var init_tag = 1
        for i in 1...4{
            button = view.viewWithTag(i) as! UIButton
            if (i == Int(correct_A)){
                button.setTitle(rectangle_answers[present_Q][0], for: .normal)
            }
            else{
                button.setTitle(rectangle_answers[present_Q][init_tag], for: .normal)
                init_tag = init_tag + 1
            }
        }
        present_Q += 1
    }
    
    // MARK: - Navigation

    // /In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
 

}
