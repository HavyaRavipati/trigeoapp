//
//  TriangleViewController.swift
//  Trigeo App
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class TriangleViewController: UIViewController {

    let triangle_questions = ["AB = BC and AB = CA, this triangle is?", "what is the angle at ABC?", "If AB is has largest, what is the largest angle?"]
    let triangle_answers = [["Equilateral", "Isosceles", "Right angle", "Not enough information!"], ["60", "30", "45", "None of these"], ["BCA", "CAB", "ABC", "None of these"]]
    var score:Int = 0
    var present_Q = 0
    var correct_A:UInt32 = 0
    
    
    @IBOutlet weak var numberLBL: UILabel!
    @IBOutlet weak var scoreLBL: UILabel!
    @IBAction func answerBTN(_ sender: AnyObject) {
        if (sender.tag == Int(correct_A)){
            print("Its a correct answer!!")
            score += 1
            scoreLBL.text! = "\(score)"
            if (present_Q != triangle_questions.count){
                nextQuestion()
            }else{
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }        }
        else{
            print("OOPS! Its a wrong answer!!!!")
            let alert = UIAlertController(title: "Oops!!", message: "The Answer is : \(triangle_answers[present_Q-1][0])", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        if (present_Q != triangle_questions.count){
            nextQuestion()
        }else{
            
            displayCorrectAnswer()
            present_Q = 0
            score = 0
            
            }
            
        }
    }
    func displayCorrectAnswer() {
        let alert = UIAlertController(title: "Congrats!!", message: "Your score is\(score)\nClick next to goto next level", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBOutlet weak var triLBL: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        nextQuestion()
    }
    func nextQuestion(){
        triLBL.text = triangle_questions[present_Q]
        numberLBL.text = String(present_Q+1)+": "
        correct_A = arc4random_uniform(4)+1
        var button:UIButton = UIButton()
        var init_tag = 1
        for i in 1...4{
            button = view.viewWithTag(i) as! UIButton
            if (i == Int(correct_A)){
                button.setTitle(triangle_answers[present_Q][0], for: .normal)
            }
            else{
                button.setTitle(triangle_answers[present_Q][init_tag], for: .normal)
                init_tag = init_tag + 1
            }
        }
        present_Q += 1
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
