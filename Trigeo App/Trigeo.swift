//
//  Trigeo.swift
//  Trigeo App
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import Foundation

struct Basic_level{
    var shape:String
    var side:Double
    var area:Double
        
    
    
    func area_equilateraltriangle(side:Double) -> Double{
        return 3*side
    }
    func perimeter_equilateraltriangle(side:Double) -> Double{
        return (sqrt(3)/4) * side * side
    }
    
    func area_square(side:Double) -> Double{
        return side*side
    }
    func perimeter_square(side:Double) -> Double{
        return 4*side
    }
    func area_rectangle(length:Double, breadth: Double) -> Double{
        return side
    }
}
