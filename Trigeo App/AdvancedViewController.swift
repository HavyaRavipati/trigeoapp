//
//  RIghtAngledTriangleViewController.swift
//  Trigeo App
//
//  Created by student on 4/4/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class AdvancedViewController: UIViewController {
    let trignometry_questions = ["If angle B is 63, What is the measure of angle A in the right triangle below? ", "In the above right angled triangle the angle at B is 30 and side BC is 4, what is the length of c", "x * sin(45)tan(60) = cos(45)tan(30)\nWhat is x equal to?"]
    let trignometry_answers = [["27", "17","30", "90"], ["8", "7", "7.5", "1"],["1/3","1/2","1","0"]]
    
    var present_Q = 0
    var correct_A:UInt32 = 0
    
    var selectedLevel : String!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        nextQuestion()
    }
    
    var score: Int = 0
    @IBOutlet weak var trignometryQLBL: UILabel!
    
    @IBOutlet weak var ScoreLBL: UILabel!
    
    @IBAction func AnswerBTN(_ sender: AnyObject) {
        
        if (sender.tag == Int(correct_A)){
            print("Its a correct answer!!")
            score += 1
            ScoreLBL.text! = "\(score)"
            if (present_Q != trignometry_questions.count){
                nextQuestion()
            }else{
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
            
        }
        else{
            print("OOPS! Its a wrong answer!!!!")
            
            if(present_Q+1 != trignometry_questions.count){
            let alert = UIAlertController(title: "Oops!!", message: "The Answer is : \(trignometry_answers[present_Q-1][0])", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            }
            if (present_Q != trignometry_questions.count){
                nextQuestion()
                
            }else{
                
                displayCorrectAnswer()
                present_Q = 0
                score = 0
                
            }
        }
        
    }
    
    @IBOutlet weak var numberLBL: UILabel!
    func displayCorrectAnswer() {
        let alert = UIAlertController(title: "Congrats!!", message: "Your score is \(score)\nClick Exit", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func nextQuestion(){
        trignometryQLBL.text = trignometry_questions[present_Q]
        numberLBL.text = String(present_Q+1)+": "
        correct_A = arc4random_uniform(4)+1
        var button:UIButton = UIButton()
        var init_tag = 1
        for i in 1...4{
            button = view.viewWithTag(i) as! UIButton
            if (i == Int(correct_A)){
                button.setTitle(trignometry_answers[present_Q][0], for: .normal)
            }
            else{
                button.setTitle(trignometry_answers[present_Q][init_tag], for: .normal)
                init_tag = init_tag + 1
            }
        }
        present_Q += 1
    }
    
    // MARK: - Navigation
    
    // /In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
