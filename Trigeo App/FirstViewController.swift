//
//  FirstViewController.swift
//  Trigeo App
//
//  Created by student on 3/11/19.
//  Copyright © 2019 Havya Ravipati. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "level")!
        cell.textLabel?.text = "" + String(Guesser.shared[indexPath.row].correctAnswer)
        cell.detailTextLabel?.text = "# Attempts: " + String(Guesser.shared[indexPath.row].numAttemptsRequired)
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    


}

